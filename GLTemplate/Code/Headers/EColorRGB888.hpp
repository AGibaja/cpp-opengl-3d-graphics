/*!
	@author �lvaro Gibaja alvarogibaja09@gmail.com
	@copyright
	Copyright 2019 �lvaro Gibaja

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/


/*!

	@class EColorRGB888
	@brief Class that defines the a RGB888 color format.
	@details
	Defines a RGB888 format color.
*/


#ifndef ECOLORRGB888_HPP
#define ECOLORRGB888_HPP


#include  <GL/glew.h>
#include  <stdio.h>
#include  <stdlib.h>


namespace Echo
{

	class EColorRGB888
	{
	public:

		///Red component.
		GLubyte r;
		///Green component.
		GLubyte g;
		///Blue component.
		GLubyte b;

		///Default init.
		EColorRGB888();
		///Color from r g b components.
		EColorRGB888(GLubyte r, GLubyte g, GLubyte b);

		///Randomize the color.
		void randomize();

		///Generate an RGB888 random color.
		EColorRGB888 generateRandomColor();


	};
}


#endif // !COLORRGB888_HPP

