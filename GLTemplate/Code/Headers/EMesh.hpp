/*!
	@author �lvaro Gibaja alvarogibaja09@gmail.com
	@copyright
	Copyright 2019 �lvaro Gibaja

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/


/*!

	@class EMesh
	@brief Class that defines the behaviour of a mesh.
	@details
	A so called mesh is made of vertices, and normals. It contains all neccessary data for
	OpenGL.
*/



#pragma once
#ifndef MESH_HPP
#define MESH_HPP

#include <vector>
#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <gl/glew.h>

#include <EShaderProgram.hpp>
#include <EShader.hpp>
#include <ETexture.hpp>
#include <ECamera.hpp>


namespace Echo
{
	class EMesh
	{
	public:
		///Default constructor that requires of a shader program and a path to the texture.
		/*!
			@param shader_program Shader program from where to get te texture sampler amd set the uvs.
			@param path Path to the file with the texture to apply.
		*/
		EMesh(EShaderProgram shader_program, std::string path);

		~EMesh();

		///Set the vertex buffer object.
		void setVbo();
		///Create a patch mesh with n cols and n rows.
		/*!
			@param cols Columns of the patch.
			@param rows Rows of the patch.
		*/
		void createPatch(int cols, int rows);

		///Add a texture to this mesh.
		/*!
			@param path Path to the texture.
		*/
		void addTexture(std::string path);
		///Build the model matrix.
		void buildModelMatrix(); 
		///Set the model matrix for this mesh.
		/*!
			@param model_matrix New model matrix of this mesh.
		*/
		void setModelMatrix(Matrix4 model_matrix); 
		///Translate this mesh.
		/*!
			@param translation How much to translate.
		*/
		void translate(glm::vec3 translation);
		///Scale this mesh. 
		/*!
			@param scale_vector How much to scale and in what axis.
		*/
		void scale(Vector3 scale_vector); 
		///Rotate this mesh. 
		/*!
			@param angle How much to rotate in radians.
			@param rotation_axis In which axis to rotate.
		*/
		void rotate(float angle, glm::vec3 rotation_axis);
		///Update the mesh.
		void update();
		///Render the mesh.
		void render();

		///Get the model matrix for this mesh.
		Matrix4 getModelMatrix(); 

		///Get the shader program attached to this mesh.
		EShaderProgram &getShaderProgram(); 


	private:
		///Shader program attached to this mesh.
		EShaderProgram shader_program;

		///Id for the vertex buffer object used by OpenGL.
		GLuint vbo_id;
		///Id for the vertex array buffer used by OpenGL.
		GLuint vao_id;
		///Get the buffer of uv coords.
		GLuint uv_coords_id;

		///Rotation angle.
		GLfloat rotation_angle = 0.0005f;

		///Texture attached to the mesh.
		ETexture *texture;

		///Vertices of the mesh.
		std::vector <GLfloat> vertices
		{
			-0.5f, -0.5f, 0.0f,
			 0.5f,  -0.5f, 0.0f,
			 0.0f,  0.5f, 0.0f,
		};

		///Uv coords of the mesh.
		std::vector <GLfloat> uv_coords
		{
			0.0f, 1.0f,
			1.0f, 1.0f,
			0.5f, 0.0f,
		};

		///Positions of the patch.
		std::vector<unsigned int> positions;

		///Shaders list attached to the program for this mesh.
		std::vector <EShader> shaders_list;

		///Initial position vector.
		Vector3 translation_vector = Vector4(0.f);

		///Initial rotation of the mesh.
		Vector3 rotation_vector = Vector3(0.0f, 0.f, 0.0f);

		///Model matrix of the mesh.
		Matrix4 model_matrix = Matrix4(1.f);

	};

}


#endif
