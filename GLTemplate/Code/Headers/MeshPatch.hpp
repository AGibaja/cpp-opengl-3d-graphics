/*!
	@warning WIP, it's in a header file because its a quick draft of indexed drawing.
	@class MeshPatch
	@brief Class that abstracts and indexed patch drawing.
	@details It's a work in progress class of what a unique mesh patch coould look like.
	It has a more optimal draw call, since its based indexed drawing. Triangle strips are also attempted.
*/

#ifndef MESHPATCH_HPP
#define MESHPATCH_HPP

#include <iostream>
#include <string>
#include <vector>
#include <tuple>

#include <GL/glew.h>


#include <glm/glm.hpp>


class Patch
{
public:
	Patch()
	{
		this->setVbo(); 
		//this->createPatch(5, 4, 0.5f);
	}

	void render()
	{
		glBindVertexArray(vao_id);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
	}

	void renderStrip()
	{
		glPolygonMode(GL_FRONT, GL_FILL);
		glPolygonMode(GL_BACK, GL_LINE);
		glBindVertexArray(vao_id);
		glDrawElements(GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
	}


private:
	std::vector <GLfloat> vertices
	{
		 0.0f, 0.0f, 0.0f,
		 0.0f, 0.5f, 0.0f,
		 0.5f, 0.5f, 0.0f,
	     0.5f, 0.0f, 0.0f,
		
		 
	};

	std::vector <GLfloat> vertices_strip;

	std::vector <GLfloat> indices_strip_2;

	std::vector <GLuint> indices
	{
		0, 1, 2,
		2, 3, 0
	};

	std::vector <GLuint> indices_strip
	{
		//0, 1, 3, 2
		3, 2, 0, 1
	};


	GLuint vbo_id; 
	GLuint vao_id; 
	GLuint ebo_id;


	void setVbo()
	{
		glGenBuffers(1, &vbo_id);
		//vao glgenvertexarrays
		glGenVertexArrays(1, &vao_id);
		glGenBuffers(1, &ebo_id); 

		glBindVertexArray(vao_id);

		glBindBuffer(GL_ARRAY_BUFFER, vbo_id);
		glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(GLfloat), this->vertices.data(), GL_STATIC_DRAW);
		
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_id);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices_strip.size() * sizeof(GLuint), this->indices_strip.data(), GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	}

	void createPatch(int rows, int columns, float separation)
	{
		
		
	}

};




class MeshPatch
{
public:
	MeshPatch(int patches_count = 20)
		: patches_count (patches_count)
	{
		this->setMeshes();
	}

	void render()
	{
		for (auto &m : patches)
		{
			m.render();
		}
	}

private:
	
	float separation = 0.05f;

	int patches_count = 0;

	std::vector <Patch> patches; 

	void setMeshes()
	{
		patches.resize(patches_count);
		Patch p; 
		
		for (int i = 0; i < patches_count; ++i)
		{
			this->patches.push_back(p); 
		}
	}
};


#endif