/*!
	@author �lvaro Gibaja alvarogibaja09@gmail.com
	@copyright
	Copyright 2019 �lvaro Gibaja

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/


/*!

	@class EShaderProgram
	@brief Class that encapsulates all data and functions needed in a shader program @see EShader
	@details This class links all compiled shaders and provides access to them.
*/


#ifndef ESHADERPROGRAM_HPP
#define ESHADERPROGRAM_HPP


#include <vector>
#include <string>
#include <cassert>

#include <EShader.hpp>



namespace Echo
{
	class EShaderProgram
	{
	public:
		///List of shaders to link into a program.
		EShaderProgram(std::vector <EShader> shaders_list);

		///Init shader program.
		void initShaderProgram();
		///Link all shaders passed in the constructor.
		void linkShaders();
		///Set the value of a float uniform in the shader program.
		/*!
			@param id Id of the uniform.
			@new_value New value for the uniform.
		*/
		void setUniformFloat(std::string id, GLfloat new_value);

		///Get the shader program id.
		GLuint getShaderProgramId(); 

		///Return the uniform float given a location,
		/*!
			@param s Identifier of the location.
		*/
		GLfloat getUniformValueFloat(std::string s);


	private:
		///Id of the shader program.
		GLuint shader_program;

		///List of shaders to be linked into a program.
		std::vector<EShader> shaders_list;

	};


}



#endif // !SHADERPROGRAM_HPP