#pragma once
#ifndef TYPES_HPP
#define TYPES_HPP

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


using Vector4 = glm::vec4; 
using Vector3 = glm::vec3; 
using Vector2 = glm::vec2; 

using Matrix4 = glm::mat4;
using Matrix3 = glm::mat3;

constexpr float PI = 3.1416;
constexpr float TAU = 2 * PI;

#endif // !TYPES_HPP
