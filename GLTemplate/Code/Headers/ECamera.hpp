/*!
	@author �lvaro Gibaja alvarogibaja09@gmail.com
	@copyright
	Copyright 2019 �lvaro Gibaja

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

/*!

	@class ECamera
	@brief Class that defines the behaviour of a camera in a scene.
	@details
	Defines the behaviour of a camera, and allows translations, rotations, scaling.
*/


#pragma once
#ifndef ECAMERA_HPP
#define ECAMERA_HPP

#include <memory>

#include <EShaderProgram.hpp>
#include <Types.hpp>



namespace Echo
{
	class ECamera
	{
	public:
		/// Initialize the camera and define its shader program and the starting position,
		/*!
			@param shader_program Shader program from where to get the view matrix.
			@param position Starting position of the camera.
		*/
		ECamera(std::shared_ptr<EShaderProgram> shader_program, Vector3 position);

		/// Set the position of the camera.
		/*!
			@param new_position new position of the camera.
		*/
		void setPosition(Vector3 new_position);

		/// Set the view matrix.
		void setViewMatrix();

		/// Translate the position of the actual camera.
		/*!
			@param t Vector to translate.
		*/
		void translate(Vector3 t);
		///Rotate the camera.
		/*!
			@param angle Angle to rotate. 
			@param rotation_axis Axis to rotate on. 
		*/
		void rotate(float angle, Vector3 rotation_axis);

		///Return the cameras position.
		Vector3 getPosition();

		///Return the view matrix of this camera.
		Matrix4 getViewMatrix();


	private:
		///Position of the camera, initialize to zero.
		Vector3 position = Vector3(0);

		///View matrix from where to init. the camera.
		Matrix4 view_matrix = Matrix4(0); 

		///Pointer to the shader program.
		std::shared_ptr<EShaderProgram> shader_program; 

	};

}



#endif // !ECAMERA_HPP
