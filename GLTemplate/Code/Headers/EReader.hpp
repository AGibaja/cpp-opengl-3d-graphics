/*!
	@author �lvaro Gibaja alvarogibaja09@gmail.com
	@copyright
	Copyright 2019 �lvaro Gibaja

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/


/*!

	@class EReader
	@brief Simple class that reads a file and stores its contents into a buffer.
	@details
	Defines a RGB888 format color.
*/


#ifndef EREADER_HPP
#define EREADER_HPP


#include <vector>
#include <string>
#include <cassert>
#include <fstream>


namespace Echo
{

	class EReader
	{
	public:
		///Initialize the file path from where to read the file.
		/*!
			@param path Path to the file that we want to read.
		*/
		EReader(std::string path);

		///Get the buffer of the file.
		std::vector <uint8_t> getBuffer();

		///Get the file length.
		size_t getLength();


	private: 
		///Buffer with the contents of the file.
		std::vector <uint8_t> buffer;

		///Length of the file that we've read.
		size_t length;


	};
}



#endif