

#ifndef ECUBE_HPP
#define ECUBE_HPP

#include <vector>
#include <Types.hpp>
#include <GL/glew.h>
#include <EShaderProgram.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


namespace Echo
{


	class ECube
	{
	public:
		///Vertices of the mesh.
		std::vector <GLfloat> vertices
		{
			-1.0f,-1.0f,-1.0f, // triangle 1 : begin
			-1.0f,-1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f, // triangle 1 : end
			1.0f, 1.0f,-1.0f, // triangle 2 : begin
			-1.0f,-1.0f,-1.0f,
			-1.0f, 1.0f,-1.0f, // triangle 2 : end
			1.0f,-1.0f, 1.0f,
			-1.0f,-1.0f,-1.0f,
			1.0f,-1.0f,-1.0f,
			1.0f, 1.0f,-1.0f,
			1.0f,-1.0f,-1.0f,
			-1.0f,-1.0f,-1.0f,
			-1.0f,-1.0f,-1.0f,
			-1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f,-1.0f,
			1.0f,-1.0f, 1.0f,
			-1.0f,-1.0f, 1.0f,
			-1.0f,-1.0f,-1.0f,
			-1.0f, 1.0f, 1.0f,
			-1.0f,-1.0f, 1.0f,
			1.0f,-1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f,-1.0f,-1.0f,
			1.0f, 1.0f,-1.0f,
			1.0f,-1.0f,-1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f,-1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f,-1.0f,
			-1.0f, 1.0f,-1.0f,
			1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f,-1.0f,
			-1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f,
			1.0f,-1.0f, 1.0f
		};

		std::vector<int> indices;

		///Shader program attached to this mesh.
		std::shared_ptr<EShaderProgram> shader_program;

		///Id for the vertex buffer object used by OpenGL.
		GLuint vbo_id;
		///Id for the vertex array buffer used by OpenGL.
		GLuint vao_id;



		GLint model_location;

<<<<<<< HEAD
		GLint custom_color; 
=======
>>>>>>> parent of 8492ee8... Multiple colors for different meshes.

		Vector3 translation_vector = Vector3(1);

		Matrix4 model_matrix = Matrix4(1);

		std::vector<GLfloat> colors
		{
			0.583f,  0.771f,  0.014f,
			0.609f,  0.115f,  0.436f,
			0.327f,  0.483f,  0.844f,
			0.822f,  0.569f,  0.201f,
			0.435f,  0.602f,  0.223f,
			0.310f,  0.747f,  0.185f,
			0.597f,  0.770f,  0.761f,
			0.559f,  0.436f,  0.730f,
			0.359f,  0.583f,  0.152f,
			0.483f,  0.596f,  0.789f,
			0.559f,  0.861f,  0.639f,
			0.195f,  0.548f,  0.859f,
			0.014f,  0.184f,  0.576f,
			0.771f,  0.328f,  0.970f,
			0.406f,  0.615f,  0.116f,
			0.676f,  0.977f,  0.133f,
			0.971f,  0.572f,  0.833f,
			0.140f,  0.616f,  0.489f,
			0.997f,  0.513f,  0.064f,
			0.945f,  0.719f,  0.592f,
			0.543f,  0.021f,  0.978f,
			0.279f,  0.317f,  0.505f,
			0.167f,  0.620f,  0.077f,
			0.347f,  0.857f,  0.137f,
			0.055f,  0.953f,  0.042f,
			0.714f,  0.505f,  0.345f,
			0.783f,  0.290f,  0.734f,
			0.722f,  0.645f,  0.174f,
			0.302f,  0.455f,  0.848f,
			0.225f,  0.587f,  0.040f,
			0.517f,  0.713f,  0.338f,
			0.053f,  0.959f,  0.120f,
			0.393f,  0.621f,  0.362f,
			0.673f,  0.211f,  0.457f,
			0.820f,  0.883f,  0.371f,
			0.982f,  0.099f,  0.879f
		};

		Matrix4 getModelMatrix()
		{
			return this->model_matrix;
		}


		std::shared_ptr<EShaderProgram> getShaderProgram()
		{
			return this->shader_program;
		}


		ECube(std::shared_ptr<EShaderProgram>program)
			: shader_program(program)
		{
			this->initBuffers();
		}


		void translate(glm::vec3 translation)
		{
			this->model_matrix = glm::translate(this->model_matrix, translation);
		}


		void scale(Vector3 scale_vector)
		{
			this->model_matrix = glm::scale(this->model_matrix, scale_vector);
		}


		void rotate(float angle, glm::vec3 rotation_axis = glm::vec3(0.0f, 1.0f, 0.0f))
		{
			this->model_matrix = glm::rotate(this->model_matrix, angle, rotation_axis);
		}


		void update()
		{
			this->model_location = glGetUniformLocation(this->getShaderProgram()->getShaderProgramId(), "model");
<<<<<<< HEAD
			glUniformMatrix4fv(model_location, 1, GL_FALSE, glm::value_ptr(this->model_matrix));	
=======
			glUniformMatrix4fv(model_location, 1, GL_FALSE, glm::value_ptr(this->model_matrix));
>>>>>>> parent of 8492ee8... Multiple colors for different meshes.
		}

		void render()
		{
<<<<<<< HEAD
			glDepthFunc(GL_LESS);
			glDrawArrays(GL_TRIANGLES, 0, 12 * 3);
=======
			glEnableVertexAttribArray(0);
			glDrawArrays(GL_TRIANGLES, 0, 12*3);
>>>>>>> parent of 8492ee8... Multiple colors for different meshes.
		}


		void buildModelMatrix()
		{
			this->model_matrix = glm::translate(this->model_matrix, this->translation_vector);
			this->model_matrix = model_matrix;

		}

		GLuint color_buffer; 

		void initBuffers()
		{

			glGenBuffers(1, &vao_id);
			glGenBuffers(1, &vbo_id);
			glGenBuffers(1, &color_buffer);
		
			glBindVertexArray(vao_id);

			glBindBuffer(GL_ARRAY_BUFFER, color_buffer);
			glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * this->colors.size(), this->colors.data(), GL_STATIC_DRAW);

			glBindBuffer(GL_ARRAY_BUFFER, vbo_id);
			glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(GLfloat), vertices.data(), GL_STATIC_DRAW);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
			glEnableVertexAttribArray(0);

			glBindBuffer(GL_ARRAY_BUFFER, this->color_buffer);
			glVertexAttribPointer(
				2,                                // attribute. No particular reason for 1, but must match the layout in the shader.
				3,                                // size
				GL_FLOAT,                         // type
				GL_FALSE,                         // normalized?
				0,                                // stride
				(void*)0                          // array buffer offset
			);
			glEnableVertexAttribArray(2);

			glUseProgram(this->shader_program->getShaderProgramId());
		}

	};
}
#endif
