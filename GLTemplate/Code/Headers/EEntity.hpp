/*!
	@author �lvaro Gibaja alvarogibaja09@gmail.com
	@copyright
	Copyright 2019 �lvaro Gibaja

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/


/*!

	@class EEntity
	@brief Class that defines the behaviour of an entity.
	@details
	The EEntity holds a reference to other parent entity, to one or more children entities.
	It also has a pointer to the mesh attached to it.
	Its supposed to be used in a scene graph.
*/



#pragma once
#ifndef EENTITY_HPP
#define EENTITY_HPP


#include <vector>
#include <memory>

#include <EMesh.hpp>


namespace Echo
{
	class EEntity
	{
	public:
		///Constructor for an entity with only a parent, or no parent by default.
		EEntity(std::shared_ptr<EEntity> parent_entity = nullptr);
		///Constructor with a parent entity and a list of children.
		/*!
			@param parent_entity Parent entity of this entity.
			@param children Child list of this entity.
		*/
		EEntity(std::shared_ptr<EEntity> parent_entity, std::vector<EEntity*> children_entities);

		///Destructor that deletes all naked pointer.
		~EEntity(); 

		///Tell this entity if it has a parent.
		/*!
			@param has_parent Does this entity have a parent?
		*/
		void hasParent(bool has_parent); 
		///Set the mesh attached to this entity.
		/*!
			@param mesh Pointer to the mesh attached.
		*/
		void setMesh(std::shared_ptr<EMesh> mesh); 
		///Set the parent entity.
		/*!
			@param parent_entity Parent entity to attach to this entity.
		*/
		void setParent(std::shared_ptr<EEntity> parent_entity);
		///Set the parent entity.
		/*!
			@param parent_entity Parent entity to attach to this entity from a naked pointer.
		*/
		void setParent(EEntity *parent_entity);
		///Add a children entity.
		/*!
			@param child Children to attach to this entity.
		*/
		void addChild(std::shared_ptr<EEntity> child);
		///Add a children entity.
		/*!
			@param child Naked point to the children to attach to this entity.
		*/
		void addChild(EEntity *child);
		///Set the list of children.
		/*!
			@param children_entities List of children entities to attach to this entity.

		*/
		void setChildren(std::vector<EEntity*> children_entities);
		///Shortcut to pudate & render.
		void processEntity();
		///Update transformations of this entity.
		void update(); 
		///Render this entity.
		void render(); 

		///Is the mesh defined?
		bool isMeshDefined(); 
		///Does this entity have a parent.
		bool hasParent();

		///Get the world transform for this entity.
		Matrix4 getWorldTransform(); 

		///Get the mesh attached to this entity.
		EMesh *getMesh(); 

		///Get the parent of this entity.
		std::shared_ptr<EEntity> getParent();

		///Get the children entities list.
		std::vector<EEntity*> getChildrenEntities();

		///Another alternative to get the mesh attached, but as a smart pointer.
		std::shared_ptr<EMesh> mesh;

		///Is this the root entity?
		bool is_root = false;
		///Id of thid entity.
		std::string id = "";

	private:
		///Is this mesh defined.
		bool mesh_defined = false;
		///Is this entity processed.
		bool is_processed = false;
		///Does this entity have a parent.
		bool has_parent = false;

		///Parent entity of this entity ;
		std::shared_ptr<EEntity> parent_entity = nullptr;

		///List of children entities.
		std::vector<EEntity*> children_entities;

		///World transform of the mesh attached to this entity.
		Matrix4 world_transform; 


	};

}



#endif