#ifndef ESPHERE_HPP
#define ESPHERE_HPP

#include <vector>
#include <Types.hpp>
#include <GL/glew.h>
#include <EShaderProgram.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


namespace Echo
{


	class ESphere
	{
	public:
		///Vertices of the mesh.
		std::vector <GLfloat> vertices;

		std::vector<int> indices; 

		///Shader program attached to this mesh.
		std::shared_ptr<EShaderProgram> shader_program;

		///Id for the vertex buffer object used by OpenGL.
		GLuint vbo_id;
		///Id for the vertex array buffer used by OpenGL.
		GLuint vao_id;

		GLint model_location;


		unsigned int ySegments = 10;
		unsigned int xSegments = 10;

		Vector3 translation_vector = Vector3(1);

		Matrix4 model_matrix = Matrix4(1); 


		Matrix4 getModelMatrix()
		{
			return this->model_matrix; 
		}


		std::shared_ptr<EShaderProgram> getShaderProgram()
		{
			return this->shader_program; 
		}


		ESphere(std::shared_ptr<EShaderProgram>program)
			: shader_program(program)
		{
			this->initSphere();

			this->initBuffers();
		}


		void translate(glm::vec3 translation)
		{
			this->model_matrix = glm::translate(this->model_matrix, translation);
		}


		void scale(Vector3 scale_vector)
		{
			this->model_matrix = glm::scale(this->model_matrix, scale_vector);
		}


		void rotate(float angle, glm::vec3 rotation_axis = glm::vec3(0.0f, 1.0f, 0.0f))
		{
			this->model_matrix = glm::rotate(this->model_matrix, angle, rotation_axis);
		}


		void update()
		{
			this->model_location = glGetUniformLocation(this->getShaderProgram()->getShaderProgramId(), "model");
			glUniformMatrix4fv(model_location, 1, GL_FALSE, glm::value_ptr(this->model_matrix));
		}

		void render()
		{
<<<<<<< HEAD
			glDrawArrays(GL_LINE_LOOP, 0, this->indices.size());

=======
			glEnableVertexAttribArray(0);
			glDrawArrays(GL_LINE_LOOP, 0, this->indices.size());
>>>>>>> parent of 8492ee8... Multiple colors for different meshes.
		}


		void buildModelMatrix()
		{
			this->model_matrix = glm::translate(this->model_matrix, this->translation_vector);
			this->model_matrix = model_matrix;

		}


		void initBuffers()
		{
			glGenBuffers(1, &vao_id);
			glGenBuffers(1, &vbo_id);

			glBindVertexArray(vao_id);

			glBindBuffer(GL_ARRAY_BUFFER, vbo_id);
			glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(GLfloat), vertices.data(), GL_STATIC_DRAW);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
			glEnableVertexAttribArray(0);

			glUseProgram(this->shader_program->getShaderProgramId());
		}


		void initSphere()
		{
			for (unsigned int y = 0; y <= ySegments; ++y)
			{
				for (unsigned int x = 0; x <= xSegments; ++x)
				{
					float xSegment = (float)x / (float)xSegments;
					float ySegment = (float)y / (float)ySegments;
					float xPos = std::cos(xSegment * TAU) * std::sin(ySegment * PI); // TAU is 2PI
					float yPos = std::cos(ySegment * PI);
					float zPos = std::sin(xSegment * TAU) * std::sin(ySegment * PI);

					vertices.push_back(xPos);
					vertices.push_back(yPos);
					vertices.push_back(zPos);
				}
			}

			bool oddRow = false;
			for (int y = 0; y < ySegments; ++y)
			{
				for (int x = 0; x < xSegments; ++x)
				{
					indices.push_back((y + 1) * (xSegments + 1) + x);
					indices.push_back(y       * (xSegments + 1) + x);
					indices.push_back(y       * (xSegments + 1) + x + 1);

					indices.push_back((y + 1) * (xSegments + 1) + x);
					indices.push_back(y       * (xSegments + 1) + x + 1);
					indices.push_back((y + 1) * (xSegments + 1) + x + 1);
				}
			}

		}

	};
}
#endif
