/*!
	@author �lvaro Gibaja alvarogibaja09@gmail.com
	@copyright
	Copyright 2019 �lvaro Gibaja

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/


/*!

	@class EScene
	@brief Scene graph that renders nodes.
	@details
	Defines the behaviour of a scene graph, that contains multiple entities and a root entity.
*/


#pragma once
#ifndef SCENE_HPP
#define SCENE_HPP

#include <EEntity.hpp>


namespace Echo
{
	class EScene
	{
	public:
		///Initialize a scene with a predefined number of nodes.
		EScene(int sz = 200);

		///Set the entities that this scene graph will have.
		void setEntities(std::vector<std::shared_ptr<EEntity>> entities);
		///Add an entity to the scene.
		void addEntity(std::shared_ptr<EEntity> ent);

		///Get a list with the entities in the scene.
		std::vector<std::shared_ptr<EEntity>> getEntities(); 


	private:
		///List of entities that the scene has.
		std::vector<std::shared_ptr<EEntity>> entities;

		///Helper variable for the scene graph, to add entities.
		int last_added = 0;
		///Number of nodes in the graph.
		int sz = 0; 

	};
}


#endif // !SCENE_HPP
