/*!
	@author �lvaro Gibaja alvarogibaja09@gmail.com
	@copyright
	Copyright 2019 �lvaro Gibaja

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/


/*!

	@class EShader
	@brief Class that defines an OpenGL shader.
	@details
	Class that allows creating and compiling OpenGL shaders from a .GLSL file.
*/



#ifndef ESHADER_HPP
#define ESHADER_HPP

#include <string>
#include <vector>
#include <iostream>


#include <EReader.hpp>
#include <GL/glew.h>


namespace Echo
{

	class EShader
	{
	public:

		///@enum ShaderType
		/*!
			@brief Enum for vertex and fragment opengl shaders.
		*/
		enum ShaderType
		{
			UNDEFINED = 0,
			VERTEX,
			FRAGMENT
		};

		
		///Shader type.
		ShaderType shader_type;

		///Initializes a shader from a source file. 
		/*!
			@param path Path to the file with the source code of the shader.
			@param shader_type Shader type of this shader type.
		*/
		EShader(std::string path, ShaderType shader_type);

		///Creat the shader.
		void createShader();
		///Compile the shader to link it afterwards.
		void compileShader();

		///Get the shader source code as a string from the file specified in the constructor.
		std::string getShaderSource();

		///Get the shader id.
		GLuint getShaderId(); 


	private:
		///Path to the file with the source code.
		std::string path;
		///Source code of the file specified in path.
		std::string source_code;

		GLuint shader_id;


	};


}


#endif // !SHADER_HPP

