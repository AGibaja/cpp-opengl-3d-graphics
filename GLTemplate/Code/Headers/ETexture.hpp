/*!
	@author �lvaro Gibaja alvarogibaja09@gmail.com
	@copyright
	Copyright 2019 �lvaro Gibaja

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/


/*!

	@class ETexture
	@brief Class that defines a texture.
	@details
	Loads the image data from a file. Create the OpenGL texture and set it so it can be sampled.
*/


#ifndef ETEXTURE_HPP
#define ETEXTURE_HPP


#include <EColorRGBA8888.hpp>


#include <random>
#include <climits>
#include <time.h>


namespace Echo
{
	class ETexture
	{
	public:
		///Initialize the texture path and load the image from the file image.
		/*!
			@param path Path to the image.
		*/
		ETexture(std::string path);

		///Create a rgba8888 texture of a specific width and height.
		/*!
			@param w Width of the texture.
			@param h Height of the texture.
		*/
		void createTextureRGBA8888(int w, int h);
		///Create a rgb888 texture of a specific width and height.
		/*!
			@param w Width of the texture.
			@param h Height of the texture.
		*/
		void createTextureRGB888(int w, int h);
		///Initialize rgba8888 texture.
		void initTextureRGBA8888();
		///Initialize rgb888 texture.
		void initTextureRGB888();

		///Get the color buffer of the loaded rgba8888.
		std::vector<EColorRGBA8888> getTexturePixelsRGBA8888();

		///Get the OpenGL texture id.
		GLuint getTextureId();


	private:
		///OpenGL texture id
		GLuint texture_id;

		///Width of the texture.
		GLsizei width = 5;
		///Height of the texture.
		GLsizei height = 5;

		///Path of the texture file.
		std::string path;

		///Rgba8888 color buffer.
		std::vector <EColorRGBA8888> pixels_RGBA8888;
		///Rgb888 color buffer.
		std::vector <EColorRGBA8888> pixels_RGB888;

	};
}



#endif // !ETEXTURE_HPP
