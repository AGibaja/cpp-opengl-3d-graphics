#include <EEntity.hpp>
#include <iostream>


namespace Echo
{

	EEntity::EEntity(std::shared_ptr<EEntity> parent_entity )
		: parent_entity(parent_entity)
	{
		
	}


	EEntity::EEntity(std::shared_ptr<EEntity> parent_entity, std::vector<EEntity*> children_entities)
		: parent_entity(parent_entity), children_entities(children_entities)
	{
		
	}


	EEntity::~EEntity()
	{
		for (auto & e : this->children_entities)
		{
			delete e;
		}
	}


	void EEntity::hasParent(bool has_parent)
	{
		this->has_parent = has_parent; 
	}


	void EEntity::setMesh(std::shared_ptr<EMesh> mesh)
	{
		this->mesh = mesh; 
		this->mesh_defined = true; 
	}


	void EEntity::setParent(std::shared_ptr<EEntity> parent_entity)
	{
		this->has_parent = true; 
		this->parent_entity = parent_entity;
		this->parent_entity->addChild(this); 
	}


	void EEntity::setParent(EEntity *parent_entity)
	{
		this->has_parent = true;
		this->parent_entity.reset(parent_entity);
	}


	void EEntity::addChild(std::shared_ptr<EEntity> child)
	{
		child->parent_entity.reset(this); 
		this->children_entities.push_back(child.get());
	}


	void EEntity::addChild(EEntity *child)
	{
		child->setParent(this);
		this->children_entities.push_back(child);
	}


	void EEntity::setChildren(std::vector<EEntity*> children_entities)
	{
		this->children_entities = children_entities;
		std::shared_ptr <EEntity> ent (this); 
		for (auto & e : children_entities)
		{
			e->setParent(ent);
		}
	}


	//Render and update the actual entity and its children
	void EEntity::processEntity()
	{
		this->mesh->update();
		this->mesh->render();
	}

	
	void EEntity::update()
	{
		if (this->hasParent())
		{
			this->world_transform = this->parent_entity->world_transform * this->mesh->getModelMatrix();
		}

		else
		{
			this->world_transform = this->mesh->getModelMatrix();
		}
			

		for (auto &e : this->children_entities)
		{
			e->update();
		}
	}


	void EEntity::render()
	{
		this->mesh->render(); 
		for (auto &e : this->children_entities)
		{
			e->render();
		}
	}


	bool EEntity::isMeshDefined()
	{
		return this->mesh_defined; 
	}


	bool EEntity::hasParent()
	{
		return this->has_parent; 
	}


	Matrix4 EEntity::getWorldTransform()
	{
		return this->world_transform; 
	}


	EMesh *EEntity::getMesh()
	{
		return this->mesh.get(); 
	}


	std::shared_ptr<EEntity> EEntity::getParent()
	{
		return this->parent_entity;
	}


	std::vector<EEntity*> EEntity::getChildrenEntities()
	{
		
		return this->children_entities;
	}

}
