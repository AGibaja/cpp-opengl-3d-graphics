#include <EReader.hpp>



namespace Echo
{
	EReader::EReader(std::string path)
	{
		std::ifstream str = std::ifstream(path);

		if (str.good())
		{
			str.seekg(0, str.end);
			this->length = str.tellg();
			str.seekg(0, str.beg);

			this->buffer = std::vector<uint8_t>(length);
			str.read((char*)&this->buffer[0], this->length);
		}

		else
			assert(str.good() && "Can't open file.");
	}

	std::vector <uint8_t> EReader::getBuffer()
	{
		return this->buffer;
	}

	size_t EReader::getLength()
	{
		return this->length;
	}
}