#include <ECamera.hpp>


namespace Echo
{
	ECamera::ECamera(std::shared_ptr<EShaderProgram> shader_program, Vector3 position)
		: position (position), shader_program (shader_program)
	{
		this->setViewMatrix(); 
	}


	void ECamera::setPosition(Vector3 new_position)
	{
		this->position = new_position; 
	}


	void ECamera::setViewMatrix()
	{
		this->view_matrix = this->getViewMatrix(); 
		GLuint view_location = glGetUniformLocation(this->shader_program->getShaderProgramId(), "view");
		glUniformMatrix4fv(view_location, 1, GL_FALSE, glm::value_ptr(this->view_matrix)); 
	}


	void ECamera::translate(Vector3 t)
	{
		this->position += t; 
	}


	void ECamera::rotate(float angle, Vector3 rotation_axis)
	{
	}


	Vector3 ECamera::getPosition()
	{
		return this->position;
	}


	Matrix4 ECamera::getViewMatrix()
	{
		Vector3 front = Vector3(0, 0, -1); 
		return glm::lookAt(this->position, this->position - front, Vector3(0.0f, 1.0f, 0.0f));
	}
 
}