#include <EMesh.hpp>


namespace Echo
{
	EMesh::EMesh(EShaderProgram shader_program, std::string path)
		: shader_program(shader_program)
	{
		this->setVbo();
		this->buildModelMatrix();
		this->addTexture(path); 
		
	}


	EMesh::~EMesh()
	{
		delete this->texture; 
		glDeleteBuffers(1, &vbo_id);
		glDeleteBuffers(1, &vao_id);
	}
	


	void EMesh::setVbo()
	{
		glGenBuffers(1, &vao_id);
		glGenBuffers(1, &vbo_id);
		glGenBuffers(1, &uv_coords_id);

		glBindVertexArray(vao_id);

		glBindBuffer(GL_ARRAY_BUFFER, vbo_id);
		glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(GLfloat), vertices.data(), GL_STATIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
		glEnableVertexAttribArray(0);


		glBindBuffer(GL_ARRAY_BUFFER, uv_coords_id);
		glBufferData(GL_ARRAY_BUFFER, this->uv_coords.size() * sizeof(GLfloat), this->uv_coords.data(),
			GL_STATIC_DRAW);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
		glEnableVertexAttribArray(1);


		glUseProgram(this->shader_program.getShaderProgramId());
		
	}


	void EMesh::createPatch(int w, int d)
	{
	
	}


	void EMesh::addTexture(std::string path)
	{
		this->texture = new ETexture(path);
		
	}


	void EMesh::buildModelMatrix()
	{
		this->model_matrix = glm::translate(this->model_matrix, this->translation_vector);
		this->setModelMatrix(this->model_matrix);
	}


	void EMesh::setModelMatrix(Matrix4 model_matrix)
	{
		this->model_matrix = model_matrix; 
	}


	void EMesh::translate(glm::vec3 translation)
	{
		this->model_matrix = glm::translate(this->model_matrix, translation);
	}


	void EMesh::scale(Vector3 scale_vector)
	{
		this->model_matrix = glm::scale(this->model_matrix, scale_vector);
	}


	void EMesh::rotate(float angle, glm::vec3 rotation_axis = glm::vec3(0.0f, 1.0f, 0.0f))
	{
		this->model_matrix = glm::rotate(this->model_matrix, angle, rotation_axis);
	}


	void EMesh::update()
	{
		
	}


	void EMesh::render()
	{
		glEnableVertexAttribArray(0);

		glBindTexture(GL_TEXTURE_2D, this->texture->getTextureId());		
		glDrawArrays(GL_TRIANGLES, 0, 3);
	}


	Matrix4 EMesh::getModelMatrix()
	{
		return this->model_matrix; 
	}


	EShaderProgram & EMesh::getShaderProgram()
	{
		return this->shader_program;
	}

}