#include <EScene.hpp>


namespace Echo
{
	EScene::EScene(int sz)
		: sz(sz)
	{
		this->entities.resize(sz); 
	}


	void EScene::setEntities(std::vector<std::shared_ptr<EEntity>> entities)
	{
		this->entities = entities;
	}

	
	void EScene::addEntity(std::shared_ptr<EEntity> ent)
	{
		if (last_added >= sz)
		{
			this->entities.push_back(ent);
		}
		else
		{
			this->entities[last_added] = ent;

		}
		this->last_added++;

		
	}


	std::vector<std::shared_ptr<EEntity>> EScene::getEntities()
	{
		return this->entities;
	}
}