#include <ETexture.hpp>
#include <iostream>
#include <cassert>

#define STB_IMAGE_IMPLEMENTATION
#include <ImageLoader.hpp>




namespace Echo
{
	ETexture::ETexture(std::string path) 
		: path(path)
	{
		this->initTextureRGBA8888();
	}


	void ETexture::createTextureRGBA8888(int w, int h)
	{
		EColorRGBA8888 col;
		int buffer_size = w * h;
		this->pixels_RGBA8888.resize(buffer_size);
		std::cout << "Pixels buffer size is: " << this->pixels_RGBA8888.size() << std::endl;

		for (int i = 0; i < buffer_size; ++i)
		{
			col.randomize();
			this->pixels_RGBA8888[i] = col;
		}


	}


	void ETexture::createTextureRGB888(int w, int h)
	{
		EColorRGBA8888 col;
		int buffer_size = w * h;
		this->pixels_RGB888.resize(buffer_size);
		std::cout << "Pixels buffer size is: " << this->pixels_RGB888.size() << std::endl;

		for (int i = 0; i < buffer_size; ++i)
		{
			col.randomize();
			this->pixels_RGB888[i] = col;
		}

	}


	void ETexture::initTextureRGBA8888()
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glGenTextures(1, &this->texture_id);
		glBindTexture(GL_TEXTURE_2D, this->texture_id);

		
		int width, height, nrChannels;

		unsigned char *data = stbi_load(this->path.c_str(), &width, &height, &nrChannels, STBI_rgb_alpha);

			std::cout << "Channels: " << nrChannels << std::endl; 
		if (!data)
			assert(1 != 1 && "Error loading image");
		this->width = width;
		this->height = height;

		/*ATENCION*/
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glGenerateMipmap(GL_TEXTURE_2D);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, this->width, this->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		stbi_image_free(data);
	}


	void ETexture::initTextureRGB888()
	{
		glGenTextures(1, &this->texture_id);
		glBindTexture(GL_TEXTURE_2D, this->texture_id);

		int width, height, nrChannels;

		unsigned char *data = stbi_load(this->path.c_str(), &width, &height, &nrChannels, 0);

		this->width = width;
		this->height = height;


		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glGenerateMipmap(GL_TEXTURE_2D);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, this->width, this->height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		stbi_image_free(data);

	}


	std::vector<EColorRGBA8888> ETexture::getTexturePixelsRGBA8888()
	{
		if (this->pixels_RGBA8888.empty())
		{
			assert(!this->pixels_RGBA8888.empty() && "Attempting to retrieve an empty texture!");
		}

		else
			return this->pixels_RGBA8888;
	}


	GLuint ETexture::getTextureId()
	{
		return this->texture_id;
	}

}