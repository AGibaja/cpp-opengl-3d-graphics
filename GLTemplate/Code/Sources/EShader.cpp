#include <EShader.hpp>



namespace Echo
{
	
	std::string path;
	std::string source_code;

	GLuint shader_id;

	EShader::ShaderType shader_type;

	EShader::EShader(std::string path, ShaderType shader_type = ShaderType::UNDEFINED)
		: path(path), shader_type(shader_type)
	{
		EReader reader(path);
		std::vector<uint8_t> src = reader.getBuffer();
		this->source_code = std::string(src.begin(), src.end());
		this->createShader();
		this->compileShader();
	}

	void EShader::createShader()
	{
		if (this->shader_type == ShaderType::UNDEFINED)
		{
			assert(this->shader_type != ShaderType::UNDEFINED && "Shader type must be defined!");
			return;
		}

		else if (this->shader_type == ShaderType::VERTEX)
		{
			this->shader_id = glCreateShader(GL_VERTEX_SHADER);
		}

		else if (this->shader_type == ShaderType::FRAGMENT)
		{
			this->shader_id = glCreateShader(GL_FRAGMENT_SHADER);
		}
		else
		{
			assert(1 != 1 && "Unknown shader_type");
			return;
		}
	}

	void EShader::compileShader()
	{
		const char * converted_source = this->source_code.c_str();
		glShaderSource(this->shader_id, 1, &converted_source, NULL);
		glCompileShader(this->shader_id);
		GLint success;
		char log[512];

		glGetShaderiv(this->shader_id, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(this->shader_id, 512, NULL, log);
			std::cout << "Error! " << log;
		}
	}

	std::string EShader::getShaderSource()
	{
		return this->source_code;
	}


	GLuint EShader::getShaderId()
	{
		return this->shader_id;
	}

}