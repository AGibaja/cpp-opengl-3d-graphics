#include <EColorRGBA8888.hpp>


namespace Echo
{
	EColorRGBA8888::EColorRGBA8888()
	{
		r = g = b = a = 255;
	}

	EColorRGBA8888::EColorRGBA8888(GLubyte r, GLubyte g, GLubyte b, GLubyte a)
		: r(r), g(g), b(b), a(a)
	{

	}

	void EColorRGBA8888::randomize()
	{
		r = GLubyte(rand() % 255);
		g = GLubyte(rand() % 255);
		b = GLubyte(rand() % 255);
		a = GLubyte(rand() % 255);
	}


	EColorRGBA8888 EColorRGBA8888::generateRandomColor()
	{
		EColorRGBA8888 new_color;
		for (int i = 0; i < 4; ++i)
		{
			GLubyte random = GLubyte(rand()) / GLubyte(INT_MAX);
			if (i == 0)
			{
				new_color.r = random;
			}

			else if (i == 1)
			{
				new_color.g = random;
			}

			else if (i == 2)
			{
				new_color.b = random;
			}

			//else if (i == 3)
			{
				new_color.a = 255;
			}

		}

		return new_color;
	}
}