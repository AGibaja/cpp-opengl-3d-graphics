#include <EShaderProgram.hpp>


namespace Echo
{

	EShaderProgram::EShaderProgram(std::vector <EShader> shaders_list)
		: shaders_list(shaders_list)
	{
		this->initShaderProgram();
		this->linkShaders();
	}


	void EShaderProgram::initShaderProgram()
	{
		this->shader_program = glCreateProgram();
	}


	void EShaderProgram::linkShaders()
	{
		for (auto s : this->shaders_list)
		{
			glAttachShader(this->shader_program, s.getShaderId());
		}

		glLinkProgram(this->shader_program);

		GLint linkage;
		GLchar log[512];

		glGetShaderiv(this->shader_program, GL_LINK_STATUS, &linkage);

		if (!linkage)
		{
			glGetShaderInfoLog(this->shader_program, 512, NULL, log);
			std::cout << "Linkage error! " << log << std::endl;
		}
	}


	void EShaderProgram::setUniformFloat(std::string id, GLfloat new_value)
	{
		GLuint loc;
		loc = glGetUniformLocation(this->shader_program, id.c_str());
		if (loc == -1)
		{
			assert(1 != 1 && "Cant find uniform specified.");
		}
		glUniform1fv(loc, 1, &new_value);
	}


	GLuint EShaderProgram::getShaderProgramId()
	{
		return this->shader_program;
	}


	GLfloat EShaderProgram::getUniformValueFloat(std::string s)
	{
		//Get the uniform location for this program.
		std::cout << "Looking for uniform: " << s << std::endl;
		GLuint location;
		GLfloat value;

		if (glGetUniformLocation(this->shader_program, s.c_str()) == -1)
		{
			assert(1 != 1 && "Error. Cant find uniform. Make sure said variable is also used, if not it will be removed.");
		}

		else
		{
			location = glGetUniformLocation(this->shader_program, s.c_str());
			std::cout << "Location found!" << std::endl;

			glGetUniformfv(this->shader_program, location, &value);
			std::cout << "Value retrieved is: " << value << std::endl;
		}

		return value;
	}

}