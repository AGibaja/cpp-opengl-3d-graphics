#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <cassert>

#include <gl/glew.h>
#include <glm/glm.hpp>

#include <SFML/Window.hpp>


#include <EShader.hpp>
#include <EScene.hpp>
#include <EEntity.hpp>
#include <EShaderProgram.hpp>
#include <ECamera.hpp>
#include <ESphere.hpp>
#include <ECube.hpp>
#include <Types.hpp>


using namespace Echo; 

//Cambiar de shader es muy costoso, hacer una lista de shader programs.

static ECube *cube; 

int main()
{
	sf::Window window(sf::VideoMode(1360, 800), "Window");
	glewInit(); 

	EShader s("../../../Resources/Shaders/vertex_shader.glsl", EShader::ShaderType::VERTEX); 
	EShader s2("../../../Resources/Shaders/fragment_shader.glsl", EShader::ShaderType::FRAGMENT);

	std::vector <EShader> shaders_list;
	shaders_list.push_back(s);
	shaders_list.push_back(s2);

	std::shared_ptr<EShaderProgram> program = std::make_shared<EShaderProgram>(shaders_list); 

<<<<<<< HEAD
	cube = new ECube(program);

	bool once = false;
=======
	ECamera *cam = new ECamera(program, Vector3(0, 0, -10)); 

	
	cube = new ECube(program);

	bool once = false; 
>>>>>>> parent of 8492ee8... Multiple colors for different meshes.

	bool running = true;
		while (window.isOpen())
		{
			sf::Event event; 
			while (window.pollEvent(event))
			{
				if (event.type == sf::Event::Closed)
				{
					window.close(); 
				}

				else if (event.type == sf::Event::KeyPressed)
				{
					
					if (event.key.code == sf::Keyboard::A)
					{
						cam->translate(Vector3(-1, 0, 0));
					}

					else if (event.key.code == sf::Keyboard::D)
					{
						cam->translate(Vector3(1, 0, 0));

					}

					else if (event.key.code == sf::Keyboard::S)
					{
						cam->translate(Vector3(0, 0, -1));

					}

					else if (event.key.code == sf::Keyboard::W)
					{
						cam->translate(Vector3(0, 0, 1));

<<<<<<< HEAD
				else if (event.key.code == sf::Keyboard::U)
				{
					cam->translate(Vector3(0, 1, 0));
				}

				else if (event.key.code == sf::Keyboard::J)
				{
					cam->translate(Vector3(0, -1, 0));
				}
			}
		};

		// Enable depth test
		glEnable(GL_DEPTH_TEST);
		// Accept fragment if it closer to the camera than the former one
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(0.f, 0.25f, 0.35f, 1.0f);


		GLuint projection_location = glGetUniformLocation(program->getShaderProgramId(), "projection");
		auto m = cam->getViewMatrix();
		auto projection_matrix = glm::perspective(glm::radians(45.0f), (float)800 / (float)600, 1.f, 100.0f);
		glUniformMatrix4fv(projection_location, 1, GL_FALSE, glm::value_ptr(projection_matrix));

		GLuint view_location = glGetUniformLocation(program->getShaderProgramId(), "view");
		glUniformMatrix4fv(view_location, 1, GL_FALSE, glm::value_ptr(m));

		cube->update();
		cube->render();
		window.display();
=======
					}

					else if (event.key.code == sf::Keyboard::U)
					{
						cam->translate(Vector3(0, 1, 0));
					}

					else if (event.key.code == sf::Keyboard::J)
					{
						cam->translate(Vector3(0, -1, 0));
					}
				}
			};
			
			glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT);


			GLuint projection_location = glGetUniformLocation(program->getShaderProgramId(), "projection");
			auto m = cam->getViewMatrix(); 
			auto projection_matrix = glm::perspective(glm::radians(45.0f), (float)800 / (float)600, 0.1f, 100.0f);
			glUniformMatrix4fv(projection_location, 1, GL_FALSE, glm::value_ptr(projection_matrix));

			GLuint view_location = glGetUniformLocation(program->getShaderProgramId(), "view");
			glUniformMatrix4fv(view_location, 1, GL_FALSE, glm::value_ptr(m));

			cube->update();
			cube->render();
>>>>>>> parent of 8492ee8... Multiple colors for different meshes.

			window.display();

		
		};
}