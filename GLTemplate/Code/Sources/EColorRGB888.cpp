#include <EColorRGB888.hpp>



namespace Echo
{

	EColorRGB888::EColorRGB888()
	{
		r = g = b = 255;
	}


	EColorRGB888::EColorRGB888(GLubyte r, GLubyte g, GLubyte b)
		: r(r), g(g), b(b)
	{

	}


	void EColorRGB888::randomize()
	{
		r = GLubyte(rand() % 255);
		g = GLubyte(rand() % 255);
		b = GLubyte(rand() % 255);
	}


	EColorRGB888 EColorRGB888::generateRandomColor()
	{
		EColorRGB888 new_color;
		for (int i = 0; i < 4; ++i)
		{
			GLubyte random = GLubyte(rand()) / GLubyte(INT_MAX);
			if (i == 0)
			{
				new_color.r = random;
			}

			else if (i == 1)
			{
				new_color.g = random;
			}

			else if (i == 2)
			{
				new_color.b = random;
			}
		}

		return new_color;
	}

}

